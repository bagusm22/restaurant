<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Psr\Log\LoggerInterface;
use \DateTime;

use App\Entity\Ingredient;

/**
 * @Route("/ingredient")
 */
class IngredientController extends AbstractController
{
    protected $request;
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("", methods={"POST"}, name="getLunch")
     */
    public function addIngredient(Request $request): Response {
        $this->logger->info(sprintf("---controller---"));
        $this->logger->info(sprintf($request->request->get('label')));

        $ingredient = new Ingredient();
        $ingredient->setTitle($request->request->get('title'));
        $ingredient->setBestBefore(\DateTime::createFromFormat('Y-m-d', $request->request->get('best-before')));
        $ingredient->setUseBy(\DateTime::createFromFormat('Y-m-d', $request->request->get('use-by')));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($ingredient);

        $entityManager->flush();

        return new JsonResponse([
            'id' => $ingredient->getId()
        ]);
    }

    /**
     * @Route("/all", methods={"GET"}, name="getAll")
     */
    public function getAll(Request $request): Ingredient {
        $entityManager = $this->getDoctrine()->getManager();
        $ingredient = $entityManager->getRepository(Ingredient::class)->find(1);
        if (!$ingredient) {
            throw new HttpException(400, "Invalid data");
        }

        $this->logger->info(sprintf("---controller---"));
        $this->logger->info($ingredient->getId());

        return $ingredient;
    }

}
