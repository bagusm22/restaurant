<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Psr\Log\LoggerInterface;
use App\Service\HelloService;

/**
 * @Route("/hello")
 */

class HelloController extends FOSRestController
{
    public function __construct(
            LoggerInterface $logger
        )
    {
        $this->logger = $logger;
    }
	/**
	 * @Route("/", name="hello")
     */
    public function indexAction(): Response
    {
        $svc = new HelloService($this->logger);
        $res = $svc->hello();
        return new JsonResponse([
            $res
        ]);
    }

    /**
	 * @Route("/coba", name="get_coba")
     */
    public function getCobaAction(): Response
    {
        return new JsonResponse([
            'hello' => 'This is a simple example of resource returned by your APIs coba'
        ]);
	}
}
