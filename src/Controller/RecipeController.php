<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Psr\Log\LoggerInterface;

use App\Entity\Recipe;
use App\Entity\RecipeIngredient;
use App\Entity\Ingredient;
use App\Service\RecipeService;

/**
 * @Route("/recipe", name="recipe")
 */
class RecipeController extends AbstractController
{
    protected $request;
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/lunch", methods={"GET"}, name="getLunch")
     */
    public function getLunch(Request $request): Array {
        $entityManager = $this->getDoctrine()->getManager();
        $recipe = $entityManager->getRepository(Recipe::class, $this->logger)->findRecipe();
        if (!$recipe) {
            throw new HttpException(400, "Invalid data");
        }
        $svc = new RecipeService($this->logger);
        $result = $svc->getLunchFromDB($recipe);

        return $result;
    }

    /**
     * @Route("/add", methods={"POST"}, name="addRecipe")
     */
    public function addRecipe(Request $request): Response {

        $entityManager = $this->getDoctrine()->getManager();

        $recipe = new Recipe();
        $recipe->setTitle($request->request->get('title'));

        $entityManager->persist($recipe);

        foreach($request->request->get('ingredients') as $val) {
            $ingredient = $entityManager->getRepository(Ingredient::class)->find($val);
            $recipeIngredient = new RecipeIngredient();
            $recipeIngredient->setRecipe($recipe);
            $recipeIngredient->setIngredient($ingredient);
            $entityManager->persist($recipeIngredient);
        }
        $entityManager->flush();

        return new JsonResponse([
            'id: ' => $recipe->getId()
        ]);
    }

}
