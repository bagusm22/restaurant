<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use App\Entity\Recipe;
use \Datetime;

class RecipeService {
    public function __construct(
        LoggerInterface $logger
        )
    {
        $this->logger = $logger;
    }
    public function getLunchFromDB(Array $recipes) {
        $date = new Datetime();
        $result = [];

        foreach ($recipes as $recipe) {
            $checkerExpired = FALSE;
            foreach ($recipe->getIngredients() as $ingredient) {
                if ($ingredient->getIngredient()->getUseBy() < $date) {
                    $checkerExpired = TRUE;
                    break;
                }
            }
            if ($checkerExpired) {
                continue;
            }
            array_push($result, $recipe);
        }
        return $result;
    }

    public function getLunchFromJson() {
        $this->logger->info(sprintf("---service function get lunch from json---"));
        
        return 0;
    }

    private function getLunch() {

    }
}