<?php

namespace App\Service;

use Psr\Log\LoggerInterface;

class HelloService {
    public function __construct(
        LoggerInterface $logger
        )
    {
        $this->logger = $logger;
    }
    public function hello() {
        $this->logger->info(sprintf("---service---"));
        return 0;
    }
}